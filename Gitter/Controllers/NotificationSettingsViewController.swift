import Foundation

class NotificationSettingsViewController : UITableViewController {

    @IBOutlet var allCell: UITableViewCell!
    @IBOutlet var announcementsCell: UITableViewCell!
    @IBOutlet var muteCell: UITableViewCell!

    var roomId: String?
    private var notificationsPath: String {
        get {
            return "/v1/user/me/rooms/\(roomId!)/settings/notifications"
        }
    }
    private var selectedIndexPath: IndexPath?
    private let app = UIApplication.shared
    private let authSession = Api().authSession()
    private var networkTask: URLSessionTask?
    private var isEnabled = false

    override func viewDidAppear(_ animated: Bool) {
        app.isNetworkActivityIndicatorVisible = true
        networkTask = authSession.dataTask(with: Api().urlWithPath(notificationsPath), completionHandler: {(data, response, error) in
            self.app.isNetworkActivityIndicatorVisible = false
            if let json = Api().parseIf200Ok(data, response: response, error: error) as? JsonObject {
                if let mode = json["mode"] as? String {
                    switch mode {
                    case "all":
                        self.selectedIndexPath = self.tableView.indexPath(for: self.allCell)
                    case "announcement":
                        self.selectedIndexPath = self.tableView.indexPath(for: self.announcementsCell)
                    case "mute":
                        self.selectedIndexPath = self.tableView.indexPath(for: self.muteCell)
                    default:
                        break
                    }
                }
            }
            self.isEnabled = true
            self.tableView.reloadData()
        })
        networkTask!.resume()
    }

    override func viewWillDisappear(_ animated: Bool) {
        networkTask?.cancel()
        app.isNetworkActivityIndicatorVisible = false
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath != selectedIndexPath else {
            tableView.deselectRow(at: indexPath, animated: true)
            return
        }

        if let mode = getMode(indexPath) {
            if (mode == "mute") {
                let alert = UIAlertController(title: "Are you sure?", message: "\"Mute\" will mark all existing messages as read.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Mute", style: .destructive, handler: { (action) in
                    self.setMode(mode)
                    self.setCellSelected(indexPath)
                    tableView.deselectRow(at: indexPath, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
                    tableView.deselectRow(at: indexPath, animated: true)
                }))

                present(alert, animated: true, completion: nil)
            } else {
                setMode(mode)
                setCellSelected(indexPath)
                tableView.deselectRow(at: indexPath, animated: true)
            }
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.accessoryType = indexPath == selectedIndexPath ? .checkmark : .none
        cell.selectionStyle = isEnabled ? .default : .none
        cell.isUserInteractionEnabled = isEnabled
        cell.textLabel?.isEnabled = isEnabled
        cell.detailTextLabel?.isEnabled = isEnabled

        return cell
    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        // really makes sure that the cells cant be selected
        return isEnabled ? indexPath : nil
    }

    private func getMode(_ indexPath: IndexPath) -> String? {
        if let cell = tableView.cellForRow(at: indexPath) {
            switch cell {
            case allCell:
                return "all"
            case announcementsCell:
                return "announcement"
            case muteCell:
                return "mute"
            default:
                return nil
            }
        }
        return nil
    }

    private func setMode(_ mode: String) {
        var request = Api().putRequest(notificationsPath)
        request.httpBody = try! JSONSerialization.data(withJSONObject: ["mode": mode], options: [])

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        networkTask = authSession.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
            self.app.isNetworkActivityIndicatorVisible = false
        })
        networkTask!.resume()
    }

    private func setCellSelected(_ indexPath: IndexPath) {
        if let selectedIndexPath = selectedIndexPath {
            tableView.cellForRow(at: selectedIndexPath)?.accessoryType = .none
        }

        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        selectedIndexPath = indexPath
    }
}
