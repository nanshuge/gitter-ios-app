import UIKit
import WebKit
import CoreData
import SlackTextViewController

class RoomViewController: UIViewController, NSDiscardableContent, WKNavigationDelegate, UIGestureRecognizerDelegate, AutocompleteDataSourceDelegate, SLKTextViewDelegate {

    @IBOutlet var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet var joinButton: UIBarButtonItem!
    @IBOutlet var moreButton: UIBarButtonItem!
    @IBOutlet var textInputbar: SLKTextInputbar!
    @IBOutlet var textInputBarToBottom: NSLayoutConstraint!
    @IBOutlet var textInputbarHeight: NSLayoutConstraint!
    @IBOutlet var textInputbarBorderHeight: NSLayoutConstraint!
    @IBOutlet var autocomplete: UITableView!
    @IBOutlet var autocompleteHeight: NSLayoutConstraint!

    var restService: RestService?
    var roomId: String?

    override var title: String? {
        didSet {
            if let title = title {
                super.title = shrinkTitleIfNecessary(title)
            }
        }
    }
    private var room: Room? {
        didSet {
            if let room = room {
                room.addObserver(self, forKeyPath: "name", options: .initial, context: nil)
                room.addObserver(self, forKeyPath: "roomMember", options: .initial, context: nil)
            }
        }
    }
    // Technically webview could be created here, but we cant get it fully prepped until viewDidLoad.
    // We only set it once it is ready so that any other async changes (room assigned, deinit, etc)
    // are made, they dont assume that the webView has been fully prepped.
    private var webView: WKWebView? {
        didSet {
            loadChatView(webView!, roomId: roomId!)
            webView!.addObserver(self, forKeyPath: "loading", options: .initial, context: nil)
            webView!.addObserver(self, forKeyPath: "URL", options: .initial, context: nil)
        }
    }
    private var ignoreKeyboard = false
    private let notificationCenter = NotificationCenter.default
    private var webViewTapGesture: UITapGestureRecognizer?
    private var autoCompleteDataSource: AutocompleteDataSource?
    private let www = URL(fileURLWithPath: Bundle.main.resourcePath! + "/www", isDirectory: true)
    private var fakeLoadingTimer: Timer?
    private let titleLengthLimit = 26
    private let uiMoc = CoreDataSingleton.sharedInstance.uiManagedObjectContext
    private var appropriateTextInputbarHeight: CGFloat {
        return (room?.roomMember ?? false) ? textInputbar.appropriateHeight : 0
    }

    deinit {
        notificationCenter.removeObserver(self)
        webView?.removeObserver(self, forKeyPath: "loading")
        webView?.removeObserver(self, forKeyPath: "URL")
        room?.removeObserver(self, forKeyPath: "name")
        room?.removeObserver(self, forKeyPath: "roomMember")
    }

    override func loadView() {
        super.loadView()

        guard roomId != nil else {
            fatalError("roomId is required")
        }

        notificationCenter.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.textViewDidChange), name: NSNotification.Name.UITextViewTextDidChange, object: nil)

        room = getRoomModel(roomId!)
        // get full room model as some propeties are missing in the live collection
        restService?.requestRoom(roomId!, completionHandler: { (error, didSaveNewData) in
            guard error == nil else {
                LogError("Failed to get full room object", error: error!)
                return
            }

            self.room = self.getRoomModel(self.roomId!)
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        moreButton.accessibilityLabel = "Room Options"

        textInputbarBorderHeight.constant = 0.5

        textInputbar.translatesAutoresizingMaskIntoConstraints = false
        textInputbar.rightButton.addTarget(self, action: #selector(self.didPressSend), for: .touchUpInside)
        textInputbar.textView.delegate = self
        textInputbarHeight.constant = appropriateTextInputbarHeight

        autoCompleteDataSource = AutocompleteDataSource(roomId: roomId!)
        autoCompleteDataSource!.delegate = self
        autocomplete.dataSource = autoCompleteDataSource
        autocomplete.delegate = autoCompleteDataSource

        webViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.didTapWebView(_:)))
        webViewTapGesture!.delegate = self

        let webView = WKWebView()
        webView.scrollView.bounces = false
        webView.isHidden = true
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        webView.isUserInteractionEnabled = true
        webView.addGestureRecognizer(webViewTapGesture!)
        view.insertSubview(webView, aboveSubview: textInputbar)
        let viewDict: [String : AnyObject] = [ "topLayoutGuide": topLayoutGuide, "webView": webView, "textInputbar": textInputbar ]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webView]|", options: [], metrics: nil, views: viewDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[topLayoutGuide][webView][textInputbar]", options: [], metrics: nil, views: viewDict))
        self.webView = webView
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        // eyeballs on
        webView?.evaluateJavaScript("window.dispatchEvent(new Event('focus'));") { (response, error) in
            LogIfError("Failed to set eyeballs on for chat view", error: error)
        }

        // show something while waiting for websocket connection
        UIApplication.shared.isNetworkActivityIndicatorVisible = true;
        fakeLoadingTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.onFakeWebsocketConnectionEstablished(_:)), userInfo: nil, repeats: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        ignoreKeyboard = true

        // eyeballs off
        webView?.evaluateJavaScript("window.dispatchEvent(new Event('blur'));") { (response, error) in
            LogIfError("Failed to set eyeballs off for chat view", error: error)
        }

        // no longer waiting for websocket connection
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
        fakeLoadingTimer?.invalidate()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if let url = room?.url {
            // Potential stat track: "pageView", url
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ignoreKeyboard = false
    }

    @IBAction func joinButtonTapped(_ sender: UIBarButtonItem) {
        restService?.joinRoom(room!.id, completionHandler: { (error, didSaveNewData) in
            LogIfError("Failed to join room", error: error)
        })
    }

    @IBAction func moreButtonTapped(_ sender: UIBarButtonItem) {
        let options = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        options.addAction(UIAlertAction(title: "Notification Settings", style: .default, handler: onNotificationSettingsTapped))
        options.addAction(UIAlertAction(title: "Mark All as Read", style: .default, handler: onMarkAllReadOptionTapped))
        if (!room!.oneToOne) {
            options.addAction(UIAlertAction(title: "Members", style: .default, handler: onMembersTapped))
            options.addAction(UIAlertAction(title: "Share", style: .default, handler: onShareOptionTapped))
            options.addAction(UIAlertAction(title: "Leave Room", style: .destructive, handler: onLeaveOptionTapped))
        }
        options.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        options.popoverPresentationController?.barButtonItem = moreButton

        present(options, animated: true, completion: nil)
    }

    // required NSDiscardableContent implimentation to stop ios from dropping the room view cache
    func beginContentAccess() -> Bool {
        return true
    }
    func endContentAccess() {}
    func discardContentIfPossible() {}
    func isContentDiscarded() -> Bool {
        // we dont destroy views. ios destroys views.
        return false
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        // WKWebView has its own gesture handling so we have to enable simultanious handling to get tap events
        return gestureRecognizer == webViewTapGesture
    }

    func didTapWebView(_ gestureRecognizer: UIGestureRecognizer) {
        let textView = textInputbar.textView

        if (textView.isFirstResponder) {
            // dismiss the keyboard
            textView.resignFirstResponder()
            // user has tapped away from textView, so hide any suggestions
            autoCompleteDataSource?.clear()
        }
    }

    func keyboardWillShow(_ notification: Notification) {
        guard !ignoreKeyboard else {
            return
        }

        let keyboardHeight = getKeyboardHeight(from: notification)
        let duration = getKeyboardAnimationDuration(from: notification)
        let options = getKeyboardAnimationOptions(from: notification)

        self.view.layoutIfNeeded()
        textInputBarToBottom.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func keyboardWillHide(_ notification: Notification) {
        guard !ignoreKeyboard else {
            return
        }

        let keyboardHeight = getKeyboardHeight(from: notification)
        let duration = getKeyboardAnimationDuration(from: notification)
        let options = getKeyboardAnimationOptions(from: notification)

        self.view.layoutIfNeeded()
        textInputBarToBottom.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func keyboardWillChangeFrame(_ notification: Notification) {
        guard !ignoreKeyboard else {
            return
        }

        let keyboardHeight = getKeyboardHeight(from: notification)
        let duration = getKeyboardAnimationDuration(from: notification)
        let options = getKeyboardAnimationOptions(from: notification)

        self.view.layoutIfNeeded()
        textInputBarToBottom.constant = keyboardHeight
        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    func textViewDidChange(_ textView: UITextView) {
        let textView = textInputbar.textView
        textInputbar.rightButton.isEnabled = canSend(textView.text)
        textInputbarHeight.constant = appropriateTextInputbarHeight
        textView.scrollRangeToVisible(textView.selectedRange)
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        let selectedRange = textView.selectedRange
        let text = textView.text ?? ""

        if let (word, _) = TextUtils.getClosestWord(text, underCursor: selectedRange) {
            autoCompleteDataSource?.suggest(word)
        } else {
            autoCompleteDataSource?.clear()
        }
    }

    func didStartAutocompleting() {
        textInputbar.textView.autocorrectionType = .no
        textInputbar.textView.spellCheckingType = .no
        // textview requires a refresh for the new options to be used
        quietlyRefreshTextViewFirstResponder()
    }

    func didFinishAutocompleting() {
        textInputbar.textView.autocorrectionType = .default
        textInputbar.textView.spellCheckingType = .default
        // textview requires a refresh for the new options to be used
        quietlyRefreshTextViewFirstResponder()
    }

    private func quietlyRefreshTextViewFirstResponder() {
        let textView = textInputbar.textView
        if (textView.isFirstResponder) {
            // if we dont ignore the keyboard, then the view will bounce due to keyboard hide triggering animations
            ignoreKeyboard = true
            textView.resignFirstResponder()
            // dont ignore the keyboard when becoming first responder, otherwise we wont adjust for any new spell check bars
            // and we'll be left with an ugly gap or hidden textview
            ignoreKeyboard = false
            textView.becomeFirstResponder()
        }
    }

    func suggestionsDidChange() {
        autocomplete.reloadData()

        self.autocompleteHeight.constant = self.autocomplete.contentSize.height
        UIView.animate(withDuration: 0.1, animations: {
            self.view.layoutIfNeeded()
            self.autocomplete.flashScrollIndicators()
        })
    }

    func didSelectSuggestion(_ text: String) {
        let textView = textInputbar.textView
        let selectedRange = textView.selectedRange
        if let (_, range) = TextUtils.getClosestWord(textView.text, underCursor: selectedRange) {
            var replacement = text
            if (range.location + range.length == textView.text.utf16.count) {
                // we're at the end of the message
                replacement = text + " "
            }

            textView.textStorage.beginEditing()
            textView.textStorage.replaceCharacters(in: range, with: replacement)
            textView.textStorage.endEditing()

            textView.selectedRange = NSMakeRange(range.location + replacement.utf16.count, 0)
        }
    }

    func didPressSend() {
        let textView = textInputbar.textView as SLKTextView

        // accept any autocorrects without losing focus
        textView.text = textView.text + " "
        textView.text = String(textView.text.characters.dropLast())

        sendMessage(textView.text)

        textInputbar.textView.slk_clearText(true)
    }

    private func canSend(_ text: String) -> Bool {
        let nonWhiteSpaceText = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let isEmpty = nonWhiteSpaceText.characters.count == 0

        return !isEmpty
    }

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url!
        let decision: WKNavigationActionPolicy

        if (isLikeRoomUrl(url)) {
            // first load or any javascript that calls location.reload()
            decision = WKNavigationActionPolicy.allow
        } else {
            UIApplication.shared.openURL(url)
            decision = WKNavigationActionPolicy.cancel;
        }

        decisionHandler(decision)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if (isLikeRoomUrl(webView.url)) {
            let authToken = LoginData().getAccessToken()!
            webView.evaluateJavaScript("window.bearerToken = '\(authToken)';") { (response, error) in
                LogIfError("Failed to set bearer token for chat view", error: error)
            }
        }
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let keyPath = keyPath {
            if let room = object as? Room {
                switch keyPath {
                case "name":
                    title = room.name
                case "roomMember":
                    navigationItem.rightBarButtonItem = room.roomMember ? moreButton : joinButton
                    textInputbarHeight.constant = appropriateTextInputbarHeight
                default:
                    break
                }
            } else if let webView = object as? WKWebView {
                switch keyPath {
                case "loading":
                    if (webView.isLoading) {
                        loadingIndicator.startAnimating()
                        webView.isHidden = true
                    } else {
                        loadingIndicator.stopAnimating()
                        webView.isHidden = false
                    }
                case "URL":
                    // in ios9, WKWebView can randomly blank out after a few minutes.
                    // the only way to know about it is that it sets its URL to nil.
                    if (webView.url == nil) {
                        NSLog("WKWebView has crashed")

                        // force webkit process to restart
                        if let roomId = roomId {
                            loadChatView(webView, roomId: roomId)
                        }
                    }
                default:
                    break
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let vc = destination as? NotificationSettingsViewController {
            vc.roomId = room!.id
        } else if let vc = destination as? MembersViewController {
            vc.roomId = room!.id
        }
    }

    func onFakeWebsocketConnectionEstablished(_ timer:Timer) {
        fakeLoadingTimer?.invalidate()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false;
    }

    private func shrinkTitleIfNecessary(_ title: String) -> String? {
        return title.characters.count > titleLengthLimit ? title.components(separatedBy: "/").last : title
    }

    private func onNotificationSettingsTapped(_ action: UIAlertAction) {
        performSegue(withIdentifier: "NotificationSettingsSegue", sender: self)
    }

    private func onMembersTapped(_ action: UIAlertAction) {
        performSegue(withIdentifier: "MembersSegue", sender: self)
    }

    private func onMarkAllReadOptionTapped(_ action: UIAlertAction!) {
        restService?.markAllAsRead(room!.id, completionHandler: { (error, didSaveNewData) in
            LogIfError("Failed to mark all messages as read", error: error)
        })
    }

    private func onShareOptionTapped(_ action: UIAlertAction!) {
        let activityItems = [
            "Join the chat in \(room!.name)",
            URL(string: "https://gitter.im/\(room!.uri)?utm_source=ios&utm_medium=link&utm_campaign=ios-share-link")!
            ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = moreButton
        self.present(activityViewController, animated: true, completion: nil)
    }

    private func onLeaveOptionTapped(_ action: UIAlertAction!) {
        let leaveAlert = UIAlertController(title: "Are you sure?", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        leaveAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        leaveAlert.addAction(UIAlertAction(title: "Leave \"\(room!.name)\"", style: .destructive, handler: { (action: UIAlertAction!) in
            self.restService?.leaveRoom(self.room!.id, completionHandler: { (error, didSaveNewData) in
                LogIfError("Failed to leave room", error: error)
            })
            let _ = self.navigationController?.popViewController(animated: true)
        }))
        leaveAlert.popoverPresentationController?.barButtonItem = moreButton

        present(leaveAlert, animated: true, completion: nil)
    }

    private func sendMessage(_ text: String) {
        webView?.evaluateJavaScript("window._sendMessage('\(text)');") { (response, error) in
            if let error = error {
                LogError("Failed to send message via webview, falling back to native", error: error)
                self.sendMessageFallback(text)
            }
        }
    }

    private func sendMessageFallback(_ text: String) {
        restService?.send(message: text, toRoomId: roomId!, completionHandler: { (error, didSaveNewData) in
            LogIfError("Failed to send message natively", error: error)
        })
    }

    private func getRoomModel(_ id: String) -> Room? {
        let fetchRequest = NSFetchRequest<Room>()
        let entity = NSEntityDescription.entity(forEntityName: "Room", in: uiMoc)
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "id == %@", id)
        var room: Room?
        do {
            let result = try uiMoc.fetch(fetchRequest)
            room = result.last
        } catch {
            LogError("Failed to get room from core data", error: error)
        }
        return room
    }

    private func loadChatView(_ webView: WKWebView, roomId: String) {
        if let url = getRoomUrl(roomId) , url != webView.url {
            webView.loadFileURL(url, allowingReadAccessTo: www)
        }
    }

    private func isLikeRoomUrl(_ urlToTest: URL?) -> Bool {
        if let pathToTest = urlToTest?.path, let roomUrlPath = getRoomUrl(roomId!)?.path {
            return pathToTest == roomUrlPath
        } else {
            return false
        }
    }

    private func getRoomUrl(_ roomId: String) -> URL? {
        var roomUrl: URL?
        let pageUrl = www.appendingPathComponent("mobile/embedded-chat.html")
        var roomUrlComponents = URLComponents(url: pageUrl, resolvingAgainstBaseURL: true)!
        roomUrlComponents.query = "troupeId=\(roomId)"
        roomUrl = roomUrlComponents.url

        return roomUrl
    }

    private func getKeyboardAnimationDuration(from notification: Notification) -> Double {
        return ((notification as NSNotification).userInfo![UIKeyboardAnimationDurationUserInfoKey]! as AnyObject).doubleValue
    }

    private func getKeyboardAnimationOptions(from notification: Notification) -> UIViewAnimationOptions {
        let animationCurve = ((notification as NSNotification).userInfo![UIKeyboardAnimationCurveUserInfoKey]! as AnyObject).uintValue
        // trust me on this...
        let animationCurveOption = UIViewAnimationOptions(rawValue: animationCurve! << 16)

        return [ animationCurveOption, .beginFromCurrentState ]
    }

    private func getKeyboardHeight(from notification: Notification) -> CGFloat {
        let frame = ((notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue
        let normalisedFrame = view.convert(frame!, to: nil)
        let croppedFrame = normalisedFrame.intersection(view.bounds)
        var height = croppedFrame.size.height

        let statusBarHeightWhenNotReceivingPhoneCall = CGFloat(20)
        let osVersion = ProcessInfo().operatingSystemVersion.majorVersion
        if ((osVersion == 9 || osVersion == 10) &&
            height > 0 &&
            UIApplication.shared.statusBarFrame.size.height > statusBarHeightWhenNotReceivingPhoneCall) {
            // ios9 and ios10 reports the wrong keyboard height when receiving a phonecall.
            height += (UIApplication.shared.statusBarFrame.size.height)
        }
        return height
    }
}
