import Foundation
import CoreData

class SuggestionsDataSource: NSObject, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    var delegate: SuggestionsDataSourceDelegate?

    private let fetchedResultsController: NSFetchedResultsController<Room>

    override init() {
        let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
        let suggestionFetchRequest = NSFetchRequest<Room>(entityName: "Room")
        suggestionFetchRequest.predicate = NSPredicate(format: "userSuggestionCollection != NULL AND userRoomCollection == NULL")
        suggestionFetchRequest.sortDescriptors = [NSSortDescriptor(key: "id", ascending: false)]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: suggestionFetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        super.init()
        fetchedResultsController.delegate = self
        try! fetchedResultsController.performFetch()
    }

    func getItemAtIndex(_ indexPath: IndexPath) -> Room {
        return fetchedResultsController.object(at: indexPath)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController.fetchedObjects?.count ?? 0
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (fetchedResultsController.fetchedObjects?.count ?? 0) > 0 ? "Suggested Rooms" : nil
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, indexPath: IndexPath) {
        let suggestion = fetchedResultsController.object(at: indexPath)

        AvatarUtils.sharedInstance.configure(cell.avatar, avatarUrl: suggestion.avatarUrl, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = suggestion.uri
        cell.sideLabel.text = nil
        cell.bottomLabel.text = nil
        cell.badgeType = .none
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        // we dont have any animations as the first time all the inserts come in, the header gets stuck partly off screen
        delegate?.didChangeSuggestions(self)
    }
}

protocol SuggestionsDataSourceDelegate {
    func didChangeSuggestions(_ suggestionsDataSource: SuggestionsDataSource)
}
