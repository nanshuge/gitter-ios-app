import Foundation

class MembersViewController: UITableViewController, UISearchResultsUpdating {

    var roomId: String?
    private let userFactory = ModelFactory<User>(entityName: "User", context: CoreDataSingleton.sharedInstance.uiManagedObjectContext)
    private let restService = RestService()
    private let loggedInUserId = LoginData().getUserId()!
    private var searchController = UISearchController(searchResultsController: nil)
    private var loadingView: UIView?
    private var limit = 50
    private var searchResults: [User]? {
        didSet {
            tableView.backgroundView = nil
            tableView.reloadData()
        }
    }
    private var members: [User]? {
        didSet {
            tableView.backgroundView = nil
            tableView.reloadData()
        }
    }
    private var isSearching = false {
        didSet {
            tableView.reloadData()
        }
    }

    override func loadView() {
        super.loadView()

        guard roomId != nil else {
            fatalError("roomId is required")
        }

        loadingView = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)![0] as? UIView
        getMoreMembers()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self

        let searchBar = searchController.searchBar
        searchBar.tintColor = Colors.rubyColor()
        tableView.tableHeaderView = searchBar
        // hide search bar when navigating
        definesPresentationContext = true

        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")

        // remove infinite separators
        tableView.tableFooterView = UIView()

        if (members == nil) {
            tableView.backgroundView = loadingView
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? searchResults?.count ?? 0 : members?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        let users = isSearching ? searchResults! : members!

        configureCell(cell, user: users[(indexPath as NSIndexPath).row])

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let users = isSearching ? searchResults! : members!
        let user = users[(indexPath as NSIndexPath).row]

        if user.id == loggedInUserId {
            let alertController = UIAlertController(title: "That's you!", message: "You're lovely, but you can't chat with youself.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Aww, OK", style: .default, handler: { (action) in
                tableView.deselectRow(at: indexPath, animated: true)
            }))
            present(alertController, animated: true, completion: nil)
        } else {
            let alertController = UIAlertController(title: "Chat privately with \(user.displayName)?", message: nil, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                tableView.deselectRow(at: indexPath, animated: true)
            }))
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                (self.splitViewController as! RootViewController).navigateTo(roomUri: user.username, sender: self) {
                    tableView.deselectRow(at: indexPath, animated: true)
                }
            }))
            present(alertController, animated: true, completion: nil)
        }
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        let currentOffset = scrollView.contentOffset.y
        let isAtBottom = currentOffset == maximumOffset

        if (!isSearching && isAtBottom && members?.count == limit) {
            getMoreMembers()
        }
    }

    func updateSearchResults(for searchController: UISearchController) {
        if let query = searchController.searchBar.text , query.characters.count > 0 {
            isSearching = true

            NetworkIndicator.sharedInstance.visible = true
            restService.searchUsers(inRoom: roomId!, query: query, completionHandler: { (error, ids) in
                NetworkIndicator.sharedInstance.visible = false
                guard let ids = ids , error == nil else {
                    return LogError("failed to search rooms via network", error: error!)
                }

                self.searchResults = try! self.userFactory.get(byIds: ids)
            })
        } else {
            isSearching = false
        }
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, user: User) {
        AvatarUtils.sharedInstance.configure(cell.avatar, avatarUrl: user.avatarUrl, size: AvatarWithBadgeCell.avatarHeight)

        cell.mainLabel.text = user.displayName
        cell.sideLabel.text = "@" + user.username
        cell.bottomLabel.text = nil
        cell.badgeType = .none
    }

    private func getMoreMembers() {
        limit += 50
        NetworkIndicator.sharedInstance.visible = true
        restService.getUsers(inRoom: roomId!, limit: limit) { (error, ids) in
            NetworkIndicator.sharedInstance.visible = false
            guard let ids = ids , error == nil else {
                self.limit -= 50
                return LogError("failed to search rooms via network", error: error!)
            }

            self.members = try! self.userFactory.get(byIds: ids)
        }
    }
}
