import Foundation
import SDWebImage

class AutocompleteDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    var delegate: AutocompleteDataSourceDelegate?

    private let restService = RestService()
    private let userFactory = ModelFactory<User>(entityName: "User", context: CoreDataSingleton.sharedInstance.uiManagedObjectContext)
    private let githubEmoji = GithubEmoji()
    private let roomId: String
    private let loggedInUserId = LoginData().getUserId()!
    private var suggestions: [AutocompleteSuggestion] = [] {
        didSet {
            delegate?.suggestionsDidChange()
        }
    }
    private var isAutocompleting: Bool = false {
        didSet(oldValue) {
            guard isAutocompleting != oldValue else {
                return
            }

            if (isAutocompleting) {
                delegate?.didStartAutocompleting()
            } else {
                delegate?.didFinishAutocompleting()
            }
        }
    }
    private var previousText = ""

    init(roomId: String) {
        self.roomId = roomId
        super.init()
    }

    func suggest(_ text: String) {
        guard text != previousText else {
            return
        }

        let prefix = String(text.characters.first ?? Character(""))

        switch prefix {
        case "@":
            isAutocompleting = true
            // we keep the old results while we wait for the server to respond, but only if it looks like the user is adding letters or backspacing
            suggestions = text.hasPrefix(previousText) || previousText.hasPrefix(text) ? suggestions : []

            let partialName = String(text.characters.dropFirst())
            getUserSuggestions(partialName, completionHandler: { (userSuggestions) in
                guard text == self.previousText else {
                    // someone typed too fast!
                    return
                }
                self.suggestions = userSuggestions
            })
        case ":":
            isAutocompleting = true
            let partialEmoji = String(text.characters.dropFirst())
            suggestions = getEmojiSuggestions(partialEmoji)
        default:
            isAutocompleting = false
            suggestions = []
        }

        previousText = text
    }

    func clear() {
        previousText = ""
        suggestions = []
        isAutocompleting = false
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggestions.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AutocompleteCell")! as! AutocompleteCell
        let suggestion = suggestions[(indexPath as NSIndexPath).row]
        cell.titleLabel.text = suggestion.title
        cell.descriptionLabel.text = suggestion.description
        let size = cell.avatarImageHeightInPixels
        cell.avatarImage.sd_setImage(with: suggestion.avatarUrl?(size), placeholderImage: nil, options: .refreshCached)

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let suggestion = suggestions[(indexPath as NSIndexPath).row]
        delegate?.didSelectSuggestion(suggestion.replacementText)
    }

    private func getUserSuggestions(_ query: String, completionHandler: @escaping ([AutocompleteSuggestion]) -> Void) {
        userSearch(query) { (users) in
            let userSuggestions = users.filter({ (user) -> Bool in
                return user.id != self.loggedInUserId
            }).map({ (user) -> AutocompleteSuggestion in
                let username = "@" + user.username
                return AutocompleteSuggestion(
                    replacementText: username,
                    title: user.displayName,
                    description: username,
                    avatarUrl: { (size) -> URL in
                        return AvatarUtils.sharedInstance.createUrl(user.avatarUrl, size: size)
                    }
                )
            })
            completionHandler(userSuggestions)
        }
    }

    private func getEmojiSuggestions(_ query: String) -> [AutocompleteSuggestion] {
        return githubEmoji.search(query).map { (emoji) -> AutocompleteSuggestion in
            return AutocompleteSuggestion(
                replacementText: ":\(emoji):",
                title: emoji,
                description: nil,
                avatarUrl: { (size) -> URL in
                    return self.githubEmoji.getImageUrl(forEmoji: emoji)
                }
            )
        }
    }

    private func userSearch(_ query: String, completionHandler: @escaping ([User]) -> Void) {
        if (query.characters.count > 0) {
            NetworkIndicator.sharedInstance.visible = true
            restService.searchUsers(inRoom: roomId, query: query, completionHandler: { (error, ids) in
                NetworkIndicator.sharedInstance.visible = false
                guard let ids = ids , error == nil else {
                    return LogError("failed to search users via network", error: error!)
                }

                completionHandler(try! self.userFactory.get(byIds: ids))
            })
        } else {
            // instead of returning no suggestions for empty string, get the (shortend) user list
            NetworkIndicator.sharedInstance.visible = true
            restService.getUsers(inRoom: roomId, completionHandler: { (error, ids) in
                NetworkIndicator.sharedInstance.visible = false
                guard let ids = ids , error == nil else {
                    return LogError("failed to get users via network", error: error!)
                }

                completionHandler(try! self.userFactory.get(byIds: ids))
            })
        }
    }
}

struct AutocompleteSuggestion {
    let replacementText: String
    let title: String
    let description: String?
    let avatarUrl: ((_ size: Int) -> URL)?
}

protocol AutocompleteDataSourceDelegate {
    func suggestionsDidChange()
    func didSelectSuggestion(_ text: String)
    func didStartAutocompleting()
    func didFinishAutocompleting()
}
