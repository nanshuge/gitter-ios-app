import Foundation

class PickRoomNameViewController: UITableViewController {

    var delegate: PickRoomNameViewControllerDelegate?
    var community: Group?
    var name: String? {
        didSet {
            reloadRepoSection()
            delegate?.didChangeName(self, name: name, repo: linkedRepo)
        }
    }
    var linkedRepo: Repo? {
        didSet {
            if linkedRepo != nil {
                name = linkedRepo!.name
                tableView.reloadData()
            }
            delegate?.didChangeName(self, name: name, repo: linkedRepo)
        }
    }

    private let TEXT_INPUT_SECTION = 0
    private let REPOS_SECTION = 1

    private var allRepos = [Repo]() {
        didSet {
            reloadRepoSection()
        }
    }
    private var filteredRepos: [Repo] {
        get {
            return allRepos.filter({ (repo) -> Bool in
                if let name = name {
                    let lcName = name.lowercased()
                    return repo.uri.lowercased().hasPrefix(lcName) || repo.name.lowercased().hasPrefix(lcName)
                } else {
                    return true
                }
            })
        }
    }

    override func loadView() {
        super.loadView()

        Api().authSession().dataTask(with: Api().urlWithPath("/v1/user/me/repos"), completionHandler: { (data, response, error) in
            if let jsonArray = Api().parseIf200Ok(data, response: response, error: error) as? [[String : AnyObject]] {
                self.allRepos = jsonArray.map({ (json) -> Repo in
                    return Repo(
                        uri: json["uri"] as! String,
                        isPrivate: json["private"] as! Bool,
                        description: json["description"] as? String
                    )
                }).filter({ (repo) -> Bool in
                    if let community = self.community , repo.owner != community.githubGroupName {
                        return false
                    } else {
                        return true
                    }
                })
            }
        }) .resume()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let linkedRepo = linkedRepo , allRepos.count == 0 {
            // add the linked repo so it immediately shows instead of waiting for the network
            allRepos = [linkedRepo]
        }
    }

    @IBAction func textFieldDidChange(_ sender: UITextField) {
        linkedRepo = nil
        name = (sender.text ?? "").characters.count > 0 ? sender.text : nil
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == REPOS_SECTION && allRepos.count > 0 {
            return "Optionally link with GitHub Repo"
        } else {
            return nil
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == TEXT_INPUT_SECTION ? 1 : filteredRepos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath as NSIndexPath).section == TEXT_INPUT_SECTION {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameEntryCell")!

            let textField = cell.viewWithTag(1) as! UITextField
            if let name = name {
                textField.text = name
            }
            textField.becomeFirstResponder()

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell")!
            let repo = filteredRepos[(indexPath as NSIndexPath).row]
            var text = repo.uri
            if repo.isPrivate {
                text += " 🔒"
            }
            cell.textLabel?.text = text
            cell.detailTextLabel?.text = repo.description
            cell.accessoryType = repo.owner == linkedRepo?.owner && repo.name == linkedRepo?.name ? .checkmark : .none
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (indexPath as NSIndexPath).section == REPOS_SECTION {
            linkedRepo = filteredRepos[(indexPath as NSIndexPath).row]
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }

    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return section == TEXT_INPUT_SECTION ? "Names must be alphanumeric with no spaces. Dashes are allowed." : nil
    }

    private func reloadRepoSection() {
        let repoSection = IndexSet(integer: REPOS_SECTION)
        UIView.setAnimationsEnabled(false)
        tableView.beginUpdates()
        tableView.deleteSections(repoSection, with: .none)
        tableView.insertSections(repoSection, with: .none)
        tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
}

protocol PickRoomNameViewControllerDelegate {
    func didChangeName(_ pickRoomNameViewController: PickRoomNameViewController, name: String?, repo: Repo?)
}
