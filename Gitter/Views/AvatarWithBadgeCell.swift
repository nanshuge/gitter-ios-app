import Foundation

class AvatarWithBadgeCell: UITableViewCell {

    static let avatarHeight = 40

    private static let unreadBadgeHeight: CGFloat = 20
    private static let unreadBadgeCornerRadius: CGFloat = unreadBadgeHeight / 2
    private static let unreadBadgeBorderWidth: CGFloat = 0
    private static let unreadBadgeRightMargin: CGFloat = 0

    private static let activityIndicatorHeight: CGFloat = 10
    private static let activityIndicatorCornerRadius: CGFloat = activityIndicatorHeight / 2
    private static let activityIndicatorBorderWidth: CGFloat = 2
    private static let activityIndicatorRightMargin: CGFloat = unreadBadgeRightMargin + (unreadBadgeHeight - activityIndicatorHeight) / 2

    private let unreadColor = Colors.caribbeanColor()
    private let mentionColor = Colors.jaffaColor()

    @IBOutlet var avatar: UIImageView!
    @IBOutlet var mainLabel: UILabel!
    @IBOutlet var sideLabel: UILabel!
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var badgeLabel: BadgeLabel!
    @IBOutlet var badgeLabelHeight: NSLayoutConstraint!
    @IBOutlet var bageLabelRightMargin: NSLayoutConstraint!

    // fix for unread badge color disappearing on selection
    // see http://stackoverflow.com/questions/6745919/uitableviewcell-subview-disappears-when-cell-is-selected
    override func setSelected(_ selected: Bool, animated: Bool) {
        let color = badgeLabel.backgroundColor
        super.setSelected(selected, animated: animated)

        if(selected) {
            badgeLabel.backgroundColor = color
        }
    }

    // fix for unread badge color disappearing on highlight
    // see http://stackoverflow.com/questions/6745919/uitableviewcell-subview-disappears-when-cell-is-selected
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        let color = badgeLabel.backgroundColor
        super.setHighlighted(highlighted, animated: animated)

        if(highlighted) {
            badgeLabel.backgroundColor = color
        }
    }

    var badgeType: BadgeType = .none {
        didSet {
            switch badgeType {
            case .none:
                badgeLabel.isHidden = true

            case .unread:
                badgeLabel.isHidden = false
                badgeLabel.layer.masksToBounds = true
                badgeLabelHeight.constant = AvatarWithBadgeCell.unreadBadgeHeight
                bageLabelRightMargin.constant = AvatarWithBadgeCell.unreadBadgeRightMargin
                badgeLabel.layer.cornerRadius = AvatarWithBadgeCell.unreadBadgeCornerRadius
                badgeLabel.layer.borderWidth = AvatarWithBadgeCell.unreadBadgeBorderWidth
                badgeLabel.backgroundColor = unreadColor

            case .mention:
                badgeLabel.isHidden = false
                badgeLabel.layer.masksToBounds = true
                badgeLabelHeight.constant = AvatarWithBadgeCell.unreadBadgeHeight
                bageLabelRightMargin.constant = AvatarWithBadgeCell.unreadBadgeRightMargin
                badgeLabel.layer.cornerRadius = AvatarWithBadgeCell.unreadBadgeCornerRadius
                badgeLabel.layer.borderWidth = AvatarWithBadgeCell.unreadBadgeBorderWidth
                badgeLabel.backgroundColor = mentionColor

            case .activityIndicator:
                badgeLabel.isHidden = false
                badgeLabel.layer.masksToBounds = true
                badgeLabelHeight.constant = AvatarWithBadgeCell.activityIndicatorHeight
                bageLabelRightMargin.constant = AvatarWithBadgeCell.activityIndicatorRightMargin
                badgeLabel.layer.cornerRadius = AvatarWithBadgeCell.activityIndicatorCornerRadius
                badgeLabel.layer.borderWidth = AvatarWithBadgeCell.activityIndicatorBorderWidth
                badgeLabel.layer.borderColor = unreadColor.cgColor
                badgeLabel.backgroundColor = UIColor.clear
            }
        }
    }

    enum BadgeType {
        case mention, unread, activityIndicator, none
    }
}
