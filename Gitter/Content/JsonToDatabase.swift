import CoreData
import class ISO8601DateFormatter.ISO8601DateFormatter

class JsonToDatabase {

    private let context: NSManagedObjectContext
    private let formatter = ISO8601DateFormatter()
    private let roomFactory: ModelFactory<Room>
    private let userFactory: ModelFactory<User>
    private let groupFactory: ModelFactory<Group>
    private let userRoomCollectionFactory: ModelFactory<UserRoomCollection>
    private let userGroupCollectionFactory: ModelFactory<UserGroupCollection>
    private let adminGroupCollectionFactory: ModelFactory<AdminGroupCollection>
    private let userSuggestionCollectionFactory: ModelFactory<UserSuggestionCollection>

    init (context: NSManagedObjectContext) {
        self.context = context
        roomFactory = ModelFactory(entityName: "Room", context: context)
        userFactory = ModelFactory(entityName: "User", context: context)
        groupFactory = ModelFactory(entityName: "Group", context: context)
        userRoomCollectionFactory = ModelFactory(entityName: "UserRoomCollection", context: context)
        userGroupCollectionFactory = ModelFactory(entityName: "UserGroupCollection", context: context)
        adminGroupCollectionFactory = ModelFactory(entityName: "AdminGroupCollection", context: context)
        userSuggestionCollectionFactory = ModelFactory(entityName: "UserSuggestionCollection", context: context)
    }

    func upsertRoomInUserRoomList(_ json: JsonObject) throws {
        let room = try upsertRooms([json]).first!
        let roomCollection = try userRoomCollectionFactory.getOrCreateSingleton()
        roomCollection.rooms.insert(room)
    }

    func upsertRoom(_ json: JsonObject) throws {
        let _ = try upsertRooms([json]).first!
    }

    func upsertRooms(_ roomsJson: [JsonObject]) throws -> [Room] {
        let jsonById = reduceById(roomsJson)
        let roomIds = Array(jsonById.keys)
        let groupIds = roomsJson.flatMap { $0["groupId"] as? String }
        let userIds = roomsJson.flatMap { ($0["user"] as? JsonObject)?["id"] as? String }

        // fetch all the db objects in one go
        // the dicts are mutable so that they can be updated with any newly
        // created objects, otherwise we could create duplicates.
        var roomsById = try roomFactory.getMapped(byIds: roomIds)
        var usersById = try userFactory.getMapped(byIds: userIds)
        var groupsById = try groupFactory.getMapped(byIds: groupIds)

        return try jsonById.map { (id, json) -> Room in
            let room = roomsById[id] ?? roomFactory.create()

            // this does a database lookup for favourites, and we're in a loop!
            // the lookup only happens for favourited rooms, so its not too bad...
            try updateRoom(room, json: json)

            if let userJson = json["user"] as? JsonObject {
                let userId = userJson["id"] as! String
                let user = usersById[userId] ?? userFactory.create()
                updateUser(user, json: userJson)
                usersById[userId] = user

                room.user = user
            }

            // group json can be missing, but group id will always be there for
            // group rooms
            if let groupId = room.groupId {
                if let groupJson = json["group"] as? JsonObject {
                    let group = groupsById[groupId] ?? groupFactory.create()
                    updateGroup(group, json: groupJson)
                    groupsById[groupId] = group
                }

                if let group = groupsById[groupId] {
                    room.group = group
                }
            }

            roomsById[id] = room

            return room
        }
    }

    func patchRoom(_ json: JsonObject) throws {
        if let room = try roomFactory.get(byId: json["id"] as! String) {
            if let name = json["name"] as? String {
                room.name = name
            }
            if let activity = json["activity"] as? Bool {
                room.activity = activity
            }
            if let lurk = json["lurk"] as? Bool {
                room.lurk = lurk
            }
            if let unreadItems = json["unreadItems"] as? NSNumber {
                room.unreadItems = unreadItems
            }
            if let mentions = json["mentions"] as? NSNumber {
                room.mentions = mentions
            }
            if let roomMember = json["roomMember"] as? Bool {
                room.roomMember = roomMember
            }
            if let lastAccessTimeString = json["lastAccessTime"] as? String {
                room.lastAccessTime = formatter.date(from: lastAccessTimeString)
                // the activity indicator api is weird
                room.activity = false
            }
            // When unfavouriting something, the api sends a null value (NSNull) instead of 0.
            // So we cant just "if let as Int" as it would fail the cast and ignore the update.
            // It sucks, I know.
            // oh, and NSNull() != nil
            if (json["favourite"] != nil) {
                try updateFavourites(room, value: json["favourite"] as? Int ?? 0)
            }
        }
    }

    func removeRoomFromUserRoomList(_ json: JsonObject) throws {
        if let room = try roomFactory.get(byId: json["id"] as! String) {
            room.userRoomCollection = nil
        }
    }

    func updateUserRoomList(_ roomsJson: [JsonObject]) throws {
        let userRoomCollection = try userRoomCollectionFactory.getOrCreateSingleton()
        let rooms = try upsertRooms(roomsJson)
        userRoomCollection.rooms = Set(rooms)
    }

    func updateRoomList(_ roomsJson: [JsonObject], forGroup groupId: String) throws {
        let rooms = try upsertRooms(roomsJson)

        if let group = try groupFactory.get(byId: groupId) {
            // the only way for a room to no longer be in this json array is if it was deleted. So lets delete these rooms!
            let deletedRooms = group.rooms.subtracting(rooms)
            deletedRooms.forEach { (room) in
                context.delete(room)
            }
            group.rooms = Set(rooms)
        }
    }

    func upsertUsers(_ usersJson: [JsonObject]) throws {
        let jsonById = reduceById(usersJson)
        let userIds = Array(jsonById.keys)

        let usersById = try userFactory.getMapped(byIds: userIds)

        jsonById.forEach { (id, json) in
            let user = usersById[id] ?? userFactory.create()
            updateUser(user, json: json)
        }
    }

    func upsertGroup(_ json: JsonObject) throws {
        let _ = try upsertGroups([json])
    }

    func updateUserGroupList(_ jsonArray: [JsonObject]) throws {
        let userGroupCollection = try userGroupCollectionFactory.getOrCreateSingleton()
        let groups = try upsertGroups(jsonArray)
        userGroupCollection.groups = Set(groups)
    }

    func updateAdminGroupList(_ jsonArray: [JsonObject]) throws {
        let adminGroupCollection = try adminGroupCollectionFactory.getOrCreateSingleton()
        let groups = try upsertGroups(jsonArray)
        adminGroupCollection.groups = Set(groups)
    }

    func upsertGroups(_ jsonArray: [JsonObject]) throws -> [Group] {
        let jsonById = reduceById(jsonArray)
        let ids = Array(jsonById.keys)

        var groupsById = try groupFactory.getMapped(byIds: ids)
        let roomsByGroupId = try getRoomsByGroupIds(ids)

        return jsonById.map({ (id, json) -> Group in
            let group = groupsById[id] ?? groupFactory.create()
            updateGroup(group, json: json)
            group.rooms = roomsByGroupId[id] ?? Set<Room>()
            // add it back in so we dont create duplicates
            groupsById[id] = group
            return group
        })
    }

    private func updateRoom(_ room: Room, json: JsonObject) throws {
        room.id = json["id"] as! String
        room.name = json["name"] as! String
        room.url = json["url"] as! String
        // uri isnt present on one to ones for some reason
        room.uri = json["uri"] as? String ?? String(room.url.characters.dropFirst())
        room.avatarUrl = json["avatarUrl"] as! String
        room.topic = json["topic"] as? String
        room.oneToOne = json["oneToOne"] as? Bool ?? false
        room.activity = json["activity"] as? Bool ?? false
        room.lurk = json["lurk"] as? Bool ?? false
        if let unreadItems = json["unreadItems"] as? NSNumber {
            room.unreadItems = unreadItems
        }
        if let mentions = json["mentions"] as? NSNumber {
            room.mentions = mentions
        }
        room.roomMember = json["roomMember"] as? Bool ?? false
        room.premium = json["premium"] as? Bool ?? false
        room.userCount = Int32(json["userCount"] as! Int)
        room.permissions_admin = (json["permissions"] as? [String : AnyObject])?["admin"] as? Bool ?? false
        if let lastAccessTimeString = json["lastAccessTime"] as? String {
            room.lastAccessTime = formatter.date(from: lastAccessTimeString)
        }
        try updateFavourites(room, value: json["favourite"] as? Int ?? 0)
        room.groupId = json["groupId"] as? String
    }

    private func updateUser(_ user: User, json: JsonObject) {
        user.id = json["id"] as! String
        user.username = json["username"] as! String
        user.displayName = json["displayName"] as! String
        user.avatarUrl = json["avatarUrl"] as! String
    }

    private func updateGroup(_ group: Group, json: JsonObject) {
        group.id = json["id"] as! String
        group.name = json["name"] as! String
        group.uri = json["uri"] as! String
        group.avatarUrl = json["avatarUrl"] as! String

        let backedBy = json["backedBy"] as! [String : AnyObject]
        group.backedBy_type = backedBy["type"] as? String
        group.backedBy_linkPath = backedBy["linkPath"] as? String
    }

    func updateSuggestions(_ json: [JsonObject]) throws {
        let userSuggestionCollection = try userSuggestionCollectionFactory.getOrCreateSingleton()
        let rooms = try upsertRooms(json)
        userSuggestionCollection.suggestions = Set(rooms)
    }

    private func reduceById(_ jsonArray: [JsonObject]) -> [String : JsonObject] {
        return jsonArray.reduce([String : JsonObject]()) { (hash, json) -> [String : JsonObject] in
            var mutableHash = hash
            mutableHash[json["id"] as! String] = json
            return mutableHash
        }
    }

    private func updateFavourites(_ room: Room, value: Int) throws {
        setRoomFavourite(room, favourite: Int32(value))

        if (room.favourite > 0) {
            // uh oh, favourites could potentially clash!
            try fixPotentialFavouritesClash(room)
        }
    }

    private func setRoomFavourite(_ room: Room, favourite: Int32) {
        room.favourite = favourite
        // Favourites need to be sorted like [1, 2, 5, 30, 1000, 0, 0, 0], which cannot be represented in a NSSortDescriptor.
        // So we set 0 to max int.
        room.favourite_comparable = room.favourite < 1 ? Int32.max : room.favourite
    }

    private func fixPotentialFavouritesClash(_ roomJustUpdated: Room) throws {
        let potentialClashNumber = roomJustUpdated.favourite

        let fetchRequest = NSFetchRequest<Room>()
        fetchRequest.entity = NSEntityDescription.entity(forEntityName: "Room", in: context)
        fetchRequest.predicate = NSPredicate(format: "favourite >= %d AND id != %@", potentialClashNumber, roomJustUpdated.id)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "favourite", ascending: true)]

        let favouriteRooms = try context.fetch(fetchRequest)

        var previousRoom = roomJustUpdated

        favouriteRooms.forEach { (room) in
            if (previousRoom.favourite == room.favourite) {
                setRoomFavourite(room, favourite: room.favourite + Int32(1))
            }

            previousRoom = room
        }
    }

    private func getRoomsByGroupId(_ groupId: String) throws -> Set<Room> {
        let fetchRequest = NSFetchRequest<Room>()
        let entity = NSEntityDescription.entity(forEntityName: "Room", in: context)
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "groupId == %@", groupId)
        return Set(try context.fetch(fetchRequest))
    }

    private func getRoomsByGroupIds(_ groupIds: [String]) throws -> [String : Set<Room>] {
        let fetchRequest = NSFetchRequest<Room>()
        let entity = NSEntityDescription.entity(forEntityName: "Room", in: context)
        fetchRequest.entity = entity
        fetchRequest.predicate = NSPredicate(format: "groupId IN %@", groupIds)
        let result = try context.fetch(fetchRequest)

        return result.reduce([String : Set<Room>](), { (hash, room) -> [String : Set<Room>] in
            var mutableHash = hash
            var set = mutableHash[room.groupId!] ?? Set<Room>()
            set.insert(room)
            mutableHash[room.groupId!] = set
            return mutableHash
        })
    }
}
